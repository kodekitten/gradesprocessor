package com.tengen;

import com.mongodb.*;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 */
public class App
{
   public static void main(String[] args) throws UnknownHostException
   {
      MongoClient mongoClient = new MongoClient("localhost");
      DB db = mongoClient.getDB("students");
      DBCollection grades = db.getCollection("grades");

      BasicDBObject query = new BasicDBObject("type","homework" );

      DBCursor cursor = grades.find(query).sort(new BasicDBObject("student_id", 1).append("score", 1));
      List<Integer> processedStudents = new ArrayList<Integer>();

      while(cursor.hasNext()){
         DBObject object = cursor.next();

         Integer student =(Integer) object.get("student_id");

         if(!processedStudents.contains(student)){
            System.out.println(object);

            grades.remove(object);
            processedStudents.add(student);
         }


      }
       cursor.close();


   }
}
